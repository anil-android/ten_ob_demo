package com.example.tenob.NEWOB;

public class ServiceProviderModel {
    private String serivce_provider_name;
    private String service_provider_url;

    ServiceProviderModel(String serivce_provider_name, String service_provider_url){
        this.serivce_provider_name = serivce_provider_name;
        this.service_provider_url = service_provider_url;
    }


    public String getSerivce_provider_name() {
        return serivce_provider_name;
    }

    public void setSerivce_provider_name(String serivce_provider_name) {
        this.serivce_provider_name = serivce_provider_name;
    }

    public String getService_provider_url() {
        return service_provider_url;
    }

    public void setService_provider_url(String service_provider_url) {
        this.service_provider_url = service_provider_url;
    }
}
