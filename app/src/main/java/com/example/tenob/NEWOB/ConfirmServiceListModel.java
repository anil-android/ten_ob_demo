package com.example.tenob.NEWOB;

public class ConfirmServiceListModel {
    private String service_name;
    private String cost;


    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
