package com.example.tenob.NEWOB;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.tenob.R;
import com.example.tenob.databinding.ItemSubServiceBinding;

import java.util.List;

public class InnerServicesAdapter extends RecyclerView.Adapter<InnerServicesAdapter.MyClass> {

    private List<InnerServicesModel> innerServicesModels;
    private List<InnerServicesModel> inner;
    LayoutInflater layoutInflater;
    ItemSubServiceBinding itemSubServiceBinding;
    AddOnAdapter addOnAdapter;
    private Context context;

    public InnerServicesAdapter(List<InnerServicesModel> innerServicesModels){
        this.innerServicesModels = innerServicesModels;
        this.inner = innerServicesModels;
    }


    @NonNull
    @Override
    public MyClass onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        layoutInflater =
                LayoutInflater.from(parent.getContext());
        itemSubServiceBinding= DataBindingUtil.inflate(layoutInflater, R.layout.item_sub_service,parent,false);

        context = parent.getContext();

        return new MyClass(itemSubServiceBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyClass myClass, int i) {
       itemSubServiceBinding.tvServiceName.setText(innerServicesModels.get(i).getService_name());
       itemSubServiceBinding.tvServiceCost.setText(innerServicesModels.get(i).getPrice());

        myClass.binding.ivServiceAdd.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Log.d("CLICKEDDDDD!!!!","CIHBDJHBHJBBHJ");
               dialog(context,inner)  ;
           }
       });
    }


    private void dialog(Context context, List<InnerServicesModel> innerServicesModels){
        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.layout_alert_add_on);
        dialog.setCancelable(true);


        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.rv_add_on);

        TextView no_thanks = (TextView) dialog.findViewById(R.id.tv_no_thanks);


        AddOnAdapter addOnAdapter = new AddOnAdapter(innerServicesModels);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(addOnAdapter);

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

       dialog.show();
    }

    @Override
    public int getItemCount() {
        return innerServicesModels.size();
    }

    public class MyClass extends RecyclerView.ViewHolder {
        ItemSubServiceBinding binding;
        public MyClass( ItemSubServiceBinding itemSubServiceBinding) {
            super(itemSubServiceBinding.getRoot());
            this.binding = itemSubServiceBinding;
        }
    }
}
