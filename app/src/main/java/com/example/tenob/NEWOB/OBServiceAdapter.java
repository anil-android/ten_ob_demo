package com.example.tenob.NEWOB;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.tenob.R;
import com.example.tenob.databinding.ItemInnerservicesLayoutBinding;

import java.util.ArrayList;
import java.util.List;

public class OBServiceAdapter extends Adapter<OBServiceAdapter.MyViewHolder> {

    LayoutInflater layoutInflater;

    ItemInnerservicesLayoutBinding itemInnerservicesLayoutBinding;

    private List<ServiceModel> serviceModels;

    private InnerServicesAdapter innerServicesAdapter;

    private ArrayList<InnerServicesModel> innerServicesModels;
    Context context;

    public  OBServiceAdapter (List<ServiceModel> serviceModels){
        this.serviceModels = serviceModels;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

         layoutInflater =
                LayoutInflater.from(parent.getContext());
         itemInnerservicesLayoutBinding=DataBindingUtil.inflate(layoutInflater, R.layout.item_innerservices_layout,parent,false);
        return new MyViewHolder(itemInnerservicesLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
      itemInnerservicesLayoutBinding.tvServiceName.setText(serviceModels.get(i).getServiceName());
        Log.d("IN_POSITION","INPOSITIOPN");

        setData();
        innerServicesAdapter = new InnerServicesAdapter(innerServicesModels);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);

        itemInnerservicesLayoutBinding.rvSubServices.setLayoutManager(mLayoutManager);
        itemInnerservicesLayoutBinding.rvSubServices.setItemAnimator(new DefaultItemAnimator());
        itemInnerservicesLayoutBinding.rvSubServices.setAdapter(innerServicesAdapter);




        myViewHolder.binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(myViewHolder.binding.rvSubServices.getVisibility()== View.GONE){
                    myViewHolder.binding.rvSubServices.setVisibility(View.VISIBLE);
                }else {
                    myViewHolder.binding.rvSubServices.setVisibility(View.GONE);
                }
            }
        });



    }




    @Override
    public int getItemCount() {
        return serviceModels.size();
    }


    private void setData(){
        innerServicesModels = new ArrayList<>();
        InnerServicesModel innerServicesModel = new InnerServicesModel("Signature shampoo blow","$35");
        innerServicesModels.add(innerServicesModel);
        InnerServicesModel innerServicesModel1 = new InnerServicesModel("Smarty Pants","$26");
        innerServicesModels.add(innerServicesModel1);
        InnerServicesModel innerServicesModel2 = new InnerServicesModel("Prescriptive & deep conditiong treatment","$45");
        innerServicesModels.add(innerServicesModel2);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ItemInnerservicesLayoutBinding binding;
        public MyViewHolder( ItemInnerservicesLayoutBinding itemInnerservicesLayoutBinding) {
            super(itemInnerservicesLayoutBinding.getRoot());
            this.binding = itemInnerservicesLayoutBinding;
        }
    }
}
