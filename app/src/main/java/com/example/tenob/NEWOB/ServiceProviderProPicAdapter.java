package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tenob.R;
import com.example.tenob.databinding.ItemAddOnServiceBinding;
import com.example.tenob.databinding.ItemSpNamePicBinding;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ServiceProviderProPicAdapter extends RecyclerView.Adapter<ServiceProviderProPicAdapter.MyViewHolder> {
    LayoutInflater layoutInflater;
    ItemSpNamePicBinding itemSpNamePicBinding;

    List<ServiceProviderModel> serviceProviderModels;

    ServiceProviderProPicAdapter(List<ServiceProviderModel> serviceProviderModels){
        this.serviceProviderModels = serviceProviderModels;
    }
    @NonNull
    @Override
    public ServiceProviderProPicAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        layoutInflater =
                LayoutInflater.from(viewGroup.getContext());
        itemSpNamePicBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_sp_name_pic,viewGroup,false);
        return new ServiceProviderProPicAdapter.MyViewHolder(itemSpNamePicBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceProviderProPicAdapter.MyViewHolder myViewHolder, int i) {


         String url = serviceProviderModels.get(i).getService_provider_url();

        Log.d("URLL",url);
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.profile_picture)
                .error(R.drawable.profile_picture)
                .into(myViewHolder.itemSpNamePicBinding.profileImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        Log.d("ERROR", String.valueOf(e.getStackTrace()));
                    }
                });



        myViewHolder.itemSpNamePicBinding.spName.setText(serviceProviderModels.get(i).getSerivce_provider_name());
    }

    @Override
    public int getItemCount() {
        return serviceProviderModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        ItemSpNamePicBinding itemSpNamePicBinding;

        public MyViewHolder(ItemSpNamePicBinding itemSpNamePicBinding) {
            super(itemSpNamePicBinding.getRoot());
            this.itemSpNamePicBinding = itemSpNamePicBinding;
        }
    }
}

