package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tenob.R;
import com.example.tenob.databinding.ItemAddOnServiceBinding;

import java.util.ArrayList;
import java.util.List;

public class AddOnAdapter extends RecyclerView.Adapter<AddOnAdapter.MyViewHolder> {
    LayoutInflater layoutInflater;
    ItemAddOnServiceBinding itemAddOnServiceBinding;

    List<InnerServicesModel> innerServicesModel;

    AddOnAdapter(List<InnerServicesModel> innerServicesModels){
        this.innerServicesModel = innerServicesModels;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        layoutInflater =
                LayoutInflater.from(viewGroup.getContext());
        itemAddOnServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_add_on_service,viewGroup,false);
        return new AddOnAdapter.MyViewHolder(itemAddOnServiceBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        itemAddOnServiceBinding.tvServiceAddName.setText(innerServicesModel.get(i).getService_name());
        itemAddOnServiceBinding.tvServiceAddCost.setText(innerServicesModel.get(i).getPrice());

    }

    @Override
    public int getItemCount() {
        return innerServicesModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        ItemAddOnServiceBinding itemAddOnServiceBinding;

        public MyViewHolder(ItemAddOnServiceBinding itemAddOnServiceBinding) {
            super(itemAddOnServiceBinding.getRoot());
            this.itemAddOnServiceBinding = itemAddOnServiceBinding;
        }
    }
}
