package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.tenob.R;


public class MianOBPageIndicatorActivity extends AppCompatActivity {
      ViewDataBinding activityMainObBinding;
      private CardServicesFragment cardServicesFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainObBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_ob);


        navigateLogonFragment();

    }

    private void navigateLogonFragment(){
        Bundle bundle = new Bundle();
        navigateFragment(new CardServicesFragment(), cardServicesFragment.TAG, bundle,this);
    }


    public static void navigateFragment(Fragment fragment, String tag, Bundle bundle, FragmentActivity fragmentActivity) {
        FragmentManager fragmentManager = fragmentActivity
                .getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.replace(R.id.main_fragment, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

}
