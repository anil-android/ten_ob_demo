package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.tenob.R;
import com.example.tenob.databinding.FragmentServicesObBinding;

import java.util.ArrayList;

import static com.example.tenob.NEWOB.MianOBPageIndicatorActivity.navigateFragment;


public class CardServicesFragment extends Fragment {

    FragmentServicesObBinding fragmentServicesObBinding;

    public static String TAG = "CardServicesFragment";

    private ArrayList<ServiceModel> arrayList;

    private OBServiceAdapter obServiceAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        fragmentServicesObBinding =    DataBindingUtil.inflate(
                inflater, R.layout.fragment_services_ob, container, false);

        setData();


        obServiceAdapter = new OBServiceAdapter(arrayList);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        fragmentServicesObBinding.rvServices.setLayoutManager(mLayoutManager);
        fragmentServicesObBinding.rvServices.setItemAnimator(new DefaultItemAnimator());
        fragmentServicesObBinding.rvServices.setAdapter(obServiceAdapter);

        fragmentServicesObBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateLogonFragment();
            }
        });


        return fragmentServicesObBinding.getRoot();
    }


    private void navigateLogonFragment(){
        Bundle bundle = new Bundle();
        navigateFragment(new ServiceProviderDateFragment(), ServiceProviderDateFragment.TAG, bundle,getActivity());
    }


    private void setData(){
        arrayList = new ArrayList<>();

        ServiceModel serviceModel = new ServiceModel("Signature shampoo blow dry bar style");
        arrayList.add(serviceModel);
        ServiceModel serviceModelOne = new ServiceModel("Smarty Pants Subscription");
        arrayList.add(serviceModelOne);
        ServiceModel serviceModelTwo = new ServiceModel("Prescriptive & deep conditiong treatment");
        arrayList.add(serviceModelTwo);

//        obServiceAdapter.notifyDataSetChanged();
    }
}
