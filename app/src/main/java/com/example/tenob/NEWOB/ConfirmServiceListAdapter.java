package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tenob.R;
import com.example.tenob.databinding.ItemConfirmServicesBinding;

import java.util.ArrayList;

public class ConfirmServiceListAdapter extends RecyclerView.Adapter<ConfirmServiceListAdapter.MyViewHolder> {

    LayoutInflater layoutInflater;

    ItemConfirmServicesBinding itemConfirmServicesBinding;

    private ArrayList<ConfirmServiceListModel> confirmServiceListModels;

    ConfirmServiceListAdapter(ArrayList<ConfirmServiceListModel> confirmServiceListModels){
        this.confirmServiceListModels = confirmServiceListModels;

    }


    @NonNull
    @Override
    public ConfirmServiceListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        layoutInflater =
                LayoutInflater.from(viewGroup.getContext());
        itemConfirmServicesBinding= DataBindingUtil.inflate(layoutInflater, R.layout.item_confirm_services,viewGroup,false);
        return new ConfirmServiceListAdapter.MyViewHolder(itemConfirmServicesBinding);    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmServiceListAdapter.MyViewHolder myViewHolder, int i) {
        itemConfirmServicesBinding.tvServiceAddName.setText(confirmServiceListModels.get(i).getService_name());

        itemConfirmServicesBinding.tvServiceAddCost.setText(confirmServiceListModels.get(i).getCost());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ItemConfirmServicesBinding itemConfirmServicesBinding;
        public MyViewHolder(ItemConfirmServicesBinding itemConfirmServicesBinding) {
            super(itemConfirmServicesBinding.getRoot());

            this.itemConfirmServicesBinding = itemConfirmServicesBinding;
        }
    }
}
