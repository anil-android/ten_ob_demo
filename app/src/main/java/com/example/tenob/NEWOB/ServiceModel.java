package com.example.tenob.NEWOB;

public class ServiceModel {
    private String serviceName;

   public  ServiceModel(String serviceName){
       this.serviceName = serviceName;
   }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
