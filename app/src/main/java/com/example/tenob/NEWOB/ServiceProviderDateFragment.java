package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tenob.R;
import com.example.tenob.databinding.FragmentServiceProviderDateBinding;

import java.util.ArrayList;

public class ServiceProviderDateFragment extends Fragment {

    FragmentServiceProviderDateBinding fragmentServiceProviderDateBinding;
    private ArrayList<ServiceProviderModel> serviceModelArrayList;
    public static String TAG = "ServiceProviderDateFragment";

    ServiceProviderProPicAdapter serviceProviderProPicAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentServiceProviderDateBinding =    DataBindingUtil.inflate(
                inflater, R.layout.fragment_service_provider_date, container, false);

        allsetData();


        serviceProviderProPicAdapter = new ServiceProviderProPicAdapter(serviceModelArrayList);

        fragmentServiceProviderDateBinding.rvServiceProvicerPic.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));

        fragmentServiceProviderDateBinding.rvServiceProvicerPic.setItemAnimator(new DefaultItemAnimator());
        fragmentServiceProviderDateBinding.rvServiceProvicerPic.setAdapter(serviceProviderProPicAdapter);



        return fragmentServiceProviderDateBinding.getRoot();
    }


    private void allsetData(){
        serviceModelArrayList = new ArrayList<>();
        ServiceProviderModel serviceProviderModel = new ServiceProviderModel("Alli","https://plus-staff.s3.amazonaws.com/5ac5482dc41c6_image1_(1).jpeg");
        serviceModelArrayList.add(serviceProviderModel);
        ServiceProviderModel serviceProviderModel1 = new ServiceProviderModel("Joselyne","https://plus-staff.s3.amazonaws.com/5a9f2208285cf_59ca74f095bd8_Joselyne_Glenview_low_res_092617.jpg");
        serviceModelArrayList.add(serviceProviderModel1);
        ServiceProviderModel serviceProviderModel2 = new ServiceProviderModel("Lindsay","https://plus-staff.s3.amazonaws.com/5afedf1cd3688_lindsay2.0.jpg");
        serviceModelArrayList.add(serviceProviderModel2);
        ServiceProviderModel serviceProviderModel3 = new ServiceProviderModel("Sydney","https://plus-staff.s3.amazonaws.com/5b59fbafade70_IMG_0012.jpg");
        serviceModelArrayList.add(serviceProviderModel3);
        ServiceProviderModel serviceProviderModel4 = new ServiceProviderModel("Jordan","https://plus-staff.s3.amazonaws.com/5bd32d26e3502_jordan.jpeg");
        serviceModelArrayList.add(serviceProviderModel4);

        ServiceProviderModel serviceProviderModel5 = new ServiceProviderModel("Rachel","https://plus-staff.s3.amazonaws.com/5beb9365c32b4_IMG_0750.jpg");
        serviceModelArrayList.add(serviceProviderModel5);
        ServiceProviderModel serviceProviderModel6 = new ServiceProviderModel("Nina","https://plus-staff.s3.amazonaws.com/58dbcd52ae717_House-Manager-Hinsdale-NINA-600x600.jpg");
        serviceModelArrayList.add(serviceProviderModel6);
        ServiceProviderModel serviceProviderModel7 = new ServiceProviderModel("Tina","https://plus-staff.s3.amazonaws.com/58dbcc02a3801_tina-600x600.jpg");
        serviceModelArrayList.add(serviceProviderModel7);
    }
}
