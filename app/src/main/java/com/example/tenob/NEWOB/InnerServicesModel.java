package com.example.tenob.NEWOB;

public class InnerServicesModel {
    private String service_name;
    private String price;

   public InnerServicesModel(String service_name,String price){
       this.price = price;
       this.service_name = service_name;
   }


    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
