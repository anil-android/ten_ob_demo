package com.example.tenob.NEWOB;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ConfirmServiceFragment extends Fragment {

    private ArrayList<ConfirmServiceListModel> confirmServiceListModelslist;
    private ArrayList<ConfirmServiceModel> confirmServiceModels;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setData();






        return super.onCreateView(inflater, container, savedInstanceState);
    }


    private void setData(){
        confirmServiceListModelslist = new ArrayList<>();
        confirmServiceModels = new ArrayList<>();

        ConfirmServiceListModel confirmServiceListModel = new ConfirmServiceListModel();
        confirmServiceListModel.setService_name("Blow Dry Bar");
        confirmServiceListModel.setCost("$35");
        confirmServiceListModelslist.add(confirmServiceListModel);

        ConfirmServiceModel confirmServiceModel =
                new ConfirmServiceModel("Alli","https://plus-staff.s3.amazonaws.com/5ac5482dc41c6_image1_(1).jpeg",
                        confirmServiceListModelslist );

        confirmServiceModels.add(confirmServiceModel);

        ConfirmServiceListModel confirmServiceListModel1 = new ConfirmServiceListModel();
        confirmServiceListModel1.setService_name("Blow Dry Bar");
        confirmServiceListModel1.setCost("$35");
        confirmServiceListModelslist.add(confirmServiceListModel1);

        ConfirmServiceModel confirmServiceModel1 =
                new ConfirmServiceModel("Alli","https://plus-staff.s3.amazonaws.com/5ac5482dc41c6_image1_(1).jpeg",
                        confirmServiceListModelslist );

        confirmServiceModels.add(confirmServiceModel1);


    }
}
