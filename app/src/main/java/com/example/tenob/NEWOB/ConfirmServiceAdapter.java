package com.example.tenob.NEWOB;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tenob.R;
import com.example.tenob.databinding.ItemConfirmApptsBinding;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ConfirmServiceAdapter extends RecyclerView.Adapter<ConfirmServiceAdapter.MyViewHolder> {
     ArrayList<ConfirmServiceModel> confirmServiceModels;

     LayoutInflater layoutInflater;
     ItemConfirmApptsBinding itemConfirmApptsBinding;


     ConfirmServiceAdapter(ArrayList<ConfirmServiceModel> confirmServiceModels){

        this.confirmServiceModels = confirmServiceModels;

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {



        layoutInflater =
                LayoutInflater.from(viewGroup.getContext());
        itemConfirmApptsBinding= DataBindingUtil.inflate(layoutInflater, R.layout.item_confirm_appts,viewGroup,false);
        return new ConfirmServiceAdapter.MyViewHolder(itemConfirmApptsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        String url = confirmServiceModels.get(i).getSp_url();
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.profile_picture)
                .error(R.drawable.profile_picture)
                .into(myViewHolder.itemConfirmApptsBinding.profileImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        Log.d("ERROR", String.valueOf(e.getStackTrace()));
                    }
                });



        myViewHolder.itemConfirmApptsBinding.spName.setText(confirmServiceModels.get(i).getSp_name());
    }

    @Override
    public int getItemCount() {
        return confirmServiceModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemConfirmApptsBinding itemConfirmApptsBinding;
         public MyViewHolder(ItemConfirmApptsBinding itemConfirmApptsBinding) {
            super(itemConfirmApptsBinding.getRoot());
            this.itemConfirmApptsBinding = itemConfirmApptsBinding;
        }
    }
}
