package com.example.tenob.NEWOB;

import java.util.ArrayList;

public class ConfirmServiceModel {
    private String sp_name;
    private String sp_url;

    private ArrayList<ConfirmServiceListModel> confirmServiceListModels;


    ConfirmServiceModel(String sp_name,String sp_url ,ArrayList<ConfirmServiceListModel> confirmServiceListModels){
        this.sp_name = sp_name;
        this.sp_url = sp_url;
        this.confirmServiceListModels = confirmServiceListModels;
    }




    public String getSp_name() {
        return sp_name;
    }

    public void setSp_name(String sp_name) {
        this.sp_name = sp_name;
    }

    public String getSp_url() {
        return sp_url;
    }

    public void setSp_url(String sp_url) {
        this.sp_url = sp_url;
    }

    public ArrayList<ConfirmServiceListModel> getConfirmServiceListModels() {
        return confirmServiceListModels;
    }

    public void setConfirmServiceListModels(ArrayList<ConfirmServiceListModel> confirmServiceListModels) {
        this.confirmServiceListModels = confirmServiceListModels;
    }
}
